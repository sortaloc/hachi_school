<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'localization', 'namespace' => 'Website', 'prefix' => Session::get('locale')], function() {

    Route::get('language/{language}', 'LangController@getlang');

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/dang-nhap', 'HomeController@login')->name('login');

    Route::get('/dang-ky', 'HomeController@register')->name('register');

    Route::get('/gioi-thieu', 'HomeController@about')->name('about');

    Route::get('/tin-tuc', 'HomeController@news')->name('news');
    Route::get('/tin-tuc/tin-nha-truong', 'HomeController@news_sub1')->name('news_sub1');
    Route::get('/tin-tuc/tin-tu-so-giao-duc', 'HomeController@news_sub2')->name('news_sub2');
    Route::get('/tin-tuc/tin-tu-bo-giao-duc', 'HomeController@news_sub3')->name('news_sub3');
    Route::get('/tin-tuc/hoc-duong', 'HomeController@news_sub4')->name('news_sub4');
    Route::get('/tin-tuc/hoc-sinh', 'HomeController@news_sub5')->name('news_sub5');
    Route::get('/tin-tuc/giai-thuong', 'HomeController@news_sub6')->name('news_sub6');
    Route::get('/tin-tuc/hoc-bong', 'HomeController@news_sub7')->name('news_sub7');
    Route::get('/tin-tuc/truong-lien-ket', 'HomeController@news_sub8')->name('news_sub8');
    Route::get('/tin-tuc/thi-tuyen-sinh', 'HomeController@news_sub9')->name('news_sub9');

    Route::get('/tai-nguyen', 'HomeController@resources')->name('resources');

    Route::get('/tai-nguyen/thu-vien-anh', 'HomeController@resources_gallery')->name('resources_gallery');

    Route::get('/su-kien', 'HomeController@event')->name('event');

    Route::get('/lien-he', 'HomeController@contact')->name('contact');

    Route::get('/cat', function() {
        return view('website.layouts.cat');
    });
});


