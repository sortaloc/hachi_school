@extends('website.layouts.default')
@section('css-custom')
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="/home/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="/home/css/responsive.css">
    <style>
        body {
            background: #f9f8f8;
        }
        .home-style4 .rs-breadcrumbs .breadcrumbs-img {
            height:360px;
            background-image:url('home/images/breadcrumbs/1.jpg');
            background-repeat: no-repeat;
            background-size: cover;
        }
        .placeholder{
            position:relative;
            width:480px;
            height:360px;
            opacity: 0.25;
        }

        .column, .blind, .blindImg {
            position:absolute;
            top:0;
            height:100%;
            box-sizing:border-box;
        }

        .blind{
            overflow:hidden;
        }

        .blindImg{
            width:100%;
            background-size:cover;
        }
    </style>
@endsection
@section('content')
    @include('website.content.contact_list')
@endsection
@section('modal-custom')
<!-- Custom Modal Start -->
<!-- Custom Modal End -->
@endsection
@section('js-custom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
<script>
    var options = {
        imgSrc1:"https://s3-us-west-2.amazonaws.com/s.cdpn.io/261873/TelephoneBanana.jpg",
        imgSrc2:"https://s3-us-west-2.amazonaws.com/s.cdpn.io/261873/TelephoneBananaInverted.jpg",
        containerName : "placeholder",
        columns:16,
        margin:3
    }

    function VenetianBlinds(defaults) {
        var cols = defaults.columns;
        var margin = defaults.margin;
        var img2 = defaults.imgSrc1;
        var img1 = defaults.imgSrc2;
        var placeholder = document.getElementsByClassName('placeholder')[0];
        var directionX, directionY;
        
        var column, blind, blindImg;
        var bgImg, rot;
        var colL;
        var colW = (placeholder.offsetWidth / cols) - margin;
        for (var i=0; i < cols; i++) {
            colL = ((colW + margin) * i);
            
            column = document.createElement('div');
            column.className = "column";
            column.style.width = colW + "px";
            column.style.left = colL + "px";
            placeholder.appendChild(column); 
            
            for (var j=0; j<4; j++)
                {
                blind = document.createElement('div');
                blind.className = "blind";
                blind.style.width = colW + "px";
                blindImg = document.createElement('div');
                blindImg.className = "blindImg";
                
                switch (j){
                    case 0:
                        TweenMax.set(blind, {rotationY: "0"});
                        bgImg = img1;
                        break;
                    case 1:
                        TweenMax.set(blind, {rotationY: "90"});
                        bgImg = img2;
                        break;
                    case 2: 
                        TweenMax.set(blind, {rotationY: "180"});
                        bgImg = img1;
                        break;              
                    case 3:
                        TweenMax.set(blind, {rotationY: "270"});
                        bgImg = img2;
                        break;
                }
                blindImg.style.width = placeholder.offsetWidth + "px";
                blindImg.style.backgroundImage = "url("+bgImg+")";
                blindImg.style.left = -colL + "px";

                column.appendChild(blind);
                blind.appendChild(blindImg);
                
                TweenMax.set(blind, { transformOrigin:"50% 50% " + -colW/2, transformStyle: "preserve-3d"});
                }
            
            TweenMax.set(column, {transformStyle:"preserve-3d", transformPerspective:1000, rotationY:0});
            
            column.addEventListener("mouseenter", function(event){
                    var elem = event.currentTarget;
                    var rotY = elem._gsTransform.rotationY;
                
                    if(directionX > 0){
                    TweenMax.to(elem, 1, {rotationY:Math.floor(rotY/90)*90+90, transformOrigin:"50% 50% -" + colW/2, ease:Back.easeOut});
                    }else{
                    TweenMax.to(elem, 1, {rotationY:Math.floor(rotY/90)*90-90, transformOrigin:"50% 50% -" + colW/2, ease:Back.easeOut});
                    }
            })
        }
        directionY = 0;
        placeholder.addEventListener('mousemove', function (event) {
            directionX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
            // directionY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;
        });
    }

    VenetianBlinds(options);
</script>
@endsection