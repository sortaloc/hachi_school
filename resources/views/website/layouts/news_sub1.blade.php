<!-- News-sub-category Section Start -->
<div class="rs-inner-blog orange-color pt-50 pb-50 md-pt-30 md-pb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="widget-area">
                    <div class="search-widget mb-30">
                        <div class="search-wrap">
                            <input type="search" placeholder="{{trans('home.news_sub.search.placeholder')}}" name="s" class="search-input" value="">
                            <button type="submit" value="{{trans('home.news_sub.search.value')}}"><i class="flaticon-search"></i></button>
                        </div>
                    </div>
                    <div class="recent-posts-widget mb-30">
                        <h3 class="widget-title">{{trans('home.news_sub.recent_posts.title')}}</h3>
                        <div class="show-featured ">
                            <div class="post-img">
                                <a href="#"><img src="https://loremflickr.com/395/250" alt=""></a>
                            </div>
                            <div class="post-desc">
                                <a href="#">{{trans('home.news_sub.recent_posts.sub_title_1')}}</a>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{trans('home.news_sub.recent_posts.date_1')}}
                                </span>
                            </div>
                        </div>
                        <div class="show-featured ">
                            <div class="post-img">
                                <a href="#"><img src="https://loremflickr.com/395/250" alt=""></a>
                            </div>
                            <div class="post-desc">
                                <a href="#">{{trans('home.news_sub.recent_posts.sub_title_1')}}</a>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{trans('home.news_sub.recent_posts.date_1')}}
                                </span>
                            </div>
                        </div>
                        <div class="show-featured ">
                            <div class="post-img">
                                <a href="#"><img src="https://loremflickr.com/395/250" alt=""></a>
                            </div>
                            <div class="post-desc">
                                <a href="#">{{trans('home.news_sub.recent_posts.sub_title_1')}}</a>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{trans('home.news_sub.recent_posts.date_1')}}
                                </span>
                            </div>
                        </div> 
                        <div class="show-featured ">
                            <div class="post-img">
                                <a href="#"><img src="https://loremflickr.com/395/250" alt=""></a>
                            </div>
                            <div class="post-desc">
                                <a href="#">{{trans('home.news_sub.recent_posts.sub_title_1')}}</a>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{trans('home.news_sub.recent_posts.date_1')}}
                                </span>
                            </div>
                        </div>
                        <div class="show-featured ">
                            <div class="post-img">
                                <a href="#"><img src="https://loremflickr.com/395/250" alt=""></a>
                            </div>
                            <div class="post-desc">
                                <a href="#">{{trans('home.news_sub.recent_posts.sub_title_1')}}</a>
                                <span class="date">
                                    <i class="fa fa-calendar"></i>
                                    {{trans('home.news_sub.recent_posts.date_1')}}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="widget-archives mb-30">
                        <h3 class="widget-title">{{trans('home.news_sub.archives.title')}}</h3>
                        <ul>
                            <li><a href="#">{{trans('home.news_sub.archives.sub_title_1')}}</a></li>
                            <li><a href="#">{{trans('home.news_sub.archives.sub_title_2')}}</a></li>
                        </ul>
                    </div>   
                    <div class="widget-archives mb-30">
                        <h3 class="widget-title">{{trans('home.news_sub.categories.title')}}</h3>
                        <ul>
                            <li><a href="/tin-tuc/tin-nha-truong">{{trans('home.menu.news.sub_1')}}</a> </li>
                            <li><a href="/tin-tuc/tin-tu-so-giao-duc">{{trans('home.menu.news.sub_2')}}</a> </li>
                            <li><a href="/tin-tuc/tin-tu-bo-giao-duc">{{trans('home.menu.news.sub_3')}}</a> </li>
                            <li><a href="/tin-tuc/hoc-duong">{{trans('home.menu.news.sub_4')}}</a> </li>
                            <li><a href="/tin-tuc/hoc-sinh">{{trans('home.menu.news.sub_5')}}</a> </li>
                            <li><a href="/tin-tuc/giai-thuong">{{trans('home.menu.news.sub_6')}}</a> </li>
                            <li><a href="/tin-tuc/hoc-bong">{{trans('home.menu.news.sub_7')}}</a> </li>
                            <li><a href="/tin-tuc/truong-lien-ket">{{trans('home.menu.news.sub_8')}}</a> </li>
                            <li><a href="/tin-tuc/thi-tuyen-sinh">{{trans('home.menu.news.sub_9')}}</a> </li>
                        </ul>
                    </div>
                    <!-- <div class="recent-posts mb-50">
                        <h3 class="widget-title">Meta</h3>
                        <ul>
                            <li><a href="#">Log in</a></li>
                            <li><a href="#">Entries feed</a></li>
                            <li><a href="#">Comments feed</a></li>
                            <li><a href="#">WordPress.org</a></li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">University while the lovely valley team work</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">High school program starting soon 2021</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">Modern School the lovely valley team work</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">While the lovely valley team work</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">This is a great source of content for anyone…</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">While the lovely valley team work</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">The Expenses You Are Thinking</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-30">
                        <div class="blog-item">
                            <div class="blog-img text-center pt-25">
                                <a href="#"><img src="https://loremflickr.com/400/250" alt=""></a>
                            </div>
                            <div class="blog-content">
                                <h3 class="blog-title"><a href="#">This is a great source of content for anyone…</a></h3>
                                <div class="blog-meta">
                                    <ul class="btm-cate">
                                        <li>
                                            <div class="blog-date">
                                                <i class="fa fa-calendar"></i> {{trans('home.news_sub.recent_posts.date_1')}}
                                            </div>
                                        </li>
                                        <li>
                                            <div class="author">
                                                <i class="fa fa-user-o"></i> Admin  
                                            </div>
                                        </li>   
                                        <li>
                                            <div class="tag-line">
                                                <i class="fa fa-book"></i>
                                                <a href="#">{{trans($breadcrumb->subtitle)}}</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blog-desc text-justify">   
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                </div>
                                <div class="blog-button text-right">
                                    <a class="blog-btn" href="#">{{trans('home.news_sub.continue_reading')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<!-- News-sub-category Section End --> 