    <!-- CTA Section Start -->
    <div id="rs-cta" class="rs-cta home11-style pt-30 pb-35 sm-pt-15 sm-pb-20">
        <div class="wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms" style="visibility: hidden; animation-duration: 2000ms; animation-delay: 300ms; animation-name: none;">
            <div class="content  text-center">
                <div class="sec-title2 mb-40 md-mb-20">
                    <h2 class="title">{{trans('home.cta.title')}}</h2>
                    <div class="desc text-justify">{{trans('home.cta.content')}}</div>
                </div>
                <div class="btn-part">
                    <a class="hachi-btn" href="#" data-toggle="tooltip" data-placement="top" title="{{trans('home.cta.contact')}}">{{trans('home.cta.contact')}}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- CTA Section End --> 