    <!-- Banner Section Start -->            
    <div id="rs-banner" class="rs-banner style9">
        <div class="container relative">
            <div class="row" style="height:440px;">
                <div class="banner-content text-center">
                    <h1 class="banner-title mb-10 ">{{trans('home.banner.title')}}</h1>
                    <div class="desc mb-30">{{trans('home.banner.slogan')}}</div>
                    <div class="banner-btn">
                        <a class="hachi-btn" href="#" data-toggle="tooltip" data-placement="top" title="{{trans('home.banner.btn')}}">{{trans('home.banner.btn')}}</a>
                    </div>
                </div>
                <div class="shape-img">
                    <div class="spiner one">
                        <img class="up-down-new" src="/home/images/banner/shape/1.png" alt="images">
                    </div>
                    <div class="spiner two">
                        <img class="left-right-new" src="/home/images/banner/shape/2.png" alt="images">
                    </div>
                    <div class="spiner three">
                        <img class="up-down-new" src="/home/images/banner/shape/3.png" alt="images">
                    </div>  
                    <div class="spiner four">
                        <img class="left-right-new" src="/home/images/banner/shape/4.png" alt="images">
                    </div>
                </div>
            </div>
            <div class="social-icon">
                <ul class="icon-cart">
                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest-p"></i></a></li>
                </ul>
            </div> 
        </div>  
    </div>
    <!-- Banner Section End -->