    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        <div class="breadcrumbs-img">
            <img src="home/images/breadcrumbs/1.jpg" alt="Breadcrumbs Image">
        </div>
        <div class="breadcrumbs-text white-color">
            <h1 class="page-title">{{trans($breadcrumb->title)}}</h1>
            <ul>
                <li>
                    <a class="active" href="/">{{trans('home.menu.home')}}</a>
                </li>
                <li>{{trans($breadcrumb->title)}}</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumbs End -->