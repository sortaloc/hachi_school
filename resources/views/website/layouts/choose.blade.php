    <!-- Choose Section Start -->
    <div id="why-choose-us" class="why-choose-us style2 gray-bg3 pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 order-last">
                    <div class="video-wrap pt-40 md-pt-30">
                        <!-- <img src="/home/images/choose/why.jpg" alt=""> -->
                        <!-- <img src="https://loremflickr.com/1024/768" alt=""> -->
                        <img src="https://picsum.photos/1024/820" alt="">
                        <a class="popup-videos" href="https://www.youtube.com/watch?v=jNDNUY3PFKk">
                            <i class="fa fa-play"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="sec-title2 mb-40">
                        <!-- <div class="sub-title">{{trans('home.choose.sub-title')}}</div> -->
                        <h2 class="title">{{trans('home.choose.title')}}</h2>
                        <div class="desc text-justify">{{trans('home.choose.content')}}</div>
                    </div>
                    <div class="facilities-two">
                        <div class="row">
                            <div class="col-lg-6 mb-40 md-mb-25 col-md-6">
                                <div class="content-part">
                                    <div class="icon-part purple-bg">
                                        <img src="/home/images/choose/icons/1.png" alt="">
                                    </div>
                                    <div class="text-part">
                                        <h4 class="title">{{trans('home.choose.why1')}}</h4> 
                                    </div>  
                                </div>
                            </div> 
                            <div class="col-lg-6 mb-40 md-mb-25 col-md-6 single-facility">
                                <div class="content-part">
                                    <div class="icon-part purple-bg">
                                        <img src="/home/images/choose/icons/2.png" alt="">
                                    </div>
                                    <div class="text-part">
                                        <h4 class="title">{{trans('home.choose.why2')}}​</h4> 
                                    </div>  
                                </div>
                            </div> 
                            <div class="col-lg-6 sm-mb-40 col-md-6 single-facility">
                                <div class="content-part">
                                    <div class="icon-part purple-bg">
                                        <img src="/home/images/choose/icons/3.png" alt="">
                                    </div>
                                    <div class="text-part">
                                        <h4 class="title">{{trans('home.choose.why3')}}​</h4> 
                                    </div>  
                                </div>
                            </div> 
                            <div class="col-lg-6 col-md-6 single-facility">
                                <div class="content-part">
                                    <div class="icon-part purple-bg">
                                        <img src="/home/images/choose/icons/4.png" alt="">
                                    </div>
                                    <div class="text-part">
                                        <h4 class="title">{{trans('home.choose.why4')}}</h4> 
                                    </div>  
                                </div>
                            </div>  
                        </div>
                    </div> 
                </div> 
            </div> 
        </div>
    </div>
    <!-- Choose Section End -->