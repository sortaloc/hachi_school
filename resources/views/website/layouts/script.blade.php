<!-- modernizr js -->
<script src="/home/js/modernizr-2.8.3.min.js"></script>
<!-- jquery latest version -->
<script src="/home/js/jquery.min.js"></script>
<!-- popper.min.js -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<!-- Bootstrap v4.4.1 js -->
<script src="/home/js/bootstrap.min.js"></script>
<!-- Menu js -->
<script src="/home/js/rsmenu-main.js"></script> 
<!-- op nav js -->
<script src="/home/js/jquery.nav.js"></script>
<!-- owl.carousel js -->
<script src="/home/js/owl.carousel.min.js"></script>
<!-- Slick js -->
<script src="/home/js/slick.min.js"></script>
<!-- isotope.pkgd.min js -->
<script src="/home/js/isotope.pkgd.min.js"></script>
<!-- imagesloaded.pkgd.min js -->
<script src="/home/js/imagesloaded.pkgd.min.js"></script>
<!-- wow js -->
<script src="/home/js/wow.min.js"></script>
<!-- Skill bar js -->
<script src="/home/js/skill.bars.jquery.js"></script>
<script src="/home/js/jquery.counterup.min.js"></script>        
<!-- counter top js -->
<script src="/home/js/waypoints.min.js"></script>
<!-- video js -->
<script src="/home/js/jquery.mb.YTPlayer.min.js"></script>
<!-- magnific popup js -->
<script src="/home/js/jquery.magnific-popup.min.js"></script>
<!-- tilt js -->
<script src="/home/js/tilt.jquery.min.js"></script>      
<!-- plugins js -->
<script src="/home/js/plugins.js"></script>
<!-- contact form js -->
<script src="/home/js/contact.form.js"></script>
<!-- main js -->
<script src="/home/js/main.js"></script>
<!-- JS: Google Tag Manager -->
<script async src="{{asset('https://www.googletagmanager.com/gtag/js?id='.$settings->gtag_id)}}"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){
    dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', '{{$settings->gtag_id}}');

    function isEmail($email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test($email);
    };

    $(document).ready(function() {
        $('a[href$="' + location.pathname + '"]').parent('li').addClass('current-menu-item');
        
        // newsletter form validation
        $('.form_error').hide();
        $(".newsletter-form button").click(function() {
            var $email = $(".newsletter-form input").val();
            if ($email=="") {
                $('#invalid_email').show();
                return false;
            } else if (isEmail($email)) {
                $('#invalid_email').html("Đăng ký nhận tin thành công!").show();
            } else {
                $('#invalid_email').html("Địa chỉ email này không hợp lệ!").show();
            };
        });
        // var $footer = $(".rs-footer.home11-style");
        // var $magic = $(".rs-footer.home11-style .magic");
        // var magicWidth = $magic.width();
        // var maxLeft = $footer.width() - magicWidth;
        // var maxTop = $footer.height() - magicWidth;
        
        // $footer.on("mousemove", function(e) {
        //     // var offset = $(this).offset();
        //     // var relativeX = e.pageX - offset.left- magicWHalf;
        //     // var relativeY = (e.pageY - offset.top - magicWHalf);
        //     // $magic.css({"left": relativeX, "top": relativeY });

        //     function blink(){
        //         $magic  .animate({ 'left':0,'top':0, 'border-radius':'100%'},2000)
        //                 .animate({ 'left': maxLeft, 'top':0},3000)
        //                 .animate({ 'left': maxLeft, 'top':maxTop, 'border-radius':'3rem'},2000)
        //                 .animate({ 'left': 0, 'top':maxTop, 'border-radius':0},3000)
        //     }
        //     setInterval(blink,200);
        // });

        // $footer.on("mouseenter", function(e) {
        //     $magic.css({"display": "block",});                
        // });

        // $footer.on("mouseleave", function(e) {
        //     $magic.css({"display": "none",});                
        // }); 
    });
</script>