    <!-- Gallery Section Start -->
    <div class="rs-gallery home11-style pb-50 md-pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 md-mb-30">
                    <div class="gallery-part">
                        <div class="gallery-img">
                            <img src="/home/images/gallery/1.jpg" alt="">
                                <div class="content-part">
                                    <h2 class="title">{{trans('home.gallery.title1')}}</h2>
                                </div>
                            <div class="gallery-info">
                                <h2 class="title-part">{{trans('home.gallery.title1')}}</h2>
                                <p>{{trans('home.gallery.content1')}}</p>
                                <div class="btn-part">
                                    <a href="/tai-nguyen/thu-vien-anh">{{trans('home.gallery.viewall')}}<i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-lg-4 md-mb-30">
                    <div class="gallery-part">
                        <div class="gallery-img">
                            <img src="/home/images/gallery/2.jpg" alt="">
                                <div class="content-part">
                                    <h2 class="title">{{trans('home.gallery.title2')}}</h2>
                                </div>
                            <div class="gallery-info">
                                <h2 class="title-part">{{trans('home.gallery.title2')}}</h2>
                                <p>{{trans('home.gallery.content2')}}</p>
                                <div class="btn-part">
                                    <a href="/tai-nguyen/thu-vien-anh">{{trans('home.gallery.viewall')}}<i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-lg-4">
                    <div class="gallery-part">
                        <div class="gallery-img">
                            <img src="/home/images/gallery/3.jpg" alt="">
                                <div class="content-part">
                                    <h2 class="title">{{trans('home.gallery.title3')}}</h2>
                                </div>
                            <div class="gallery-info">
                                <h2 class="title-part">{{trans('home.gallery.title3')}}</h2>
                                <p>{{trans('home.gallery.content3')}}</p>
                                <div class="btn-part">
                                    <a href="/tai-nguyen/thu-vien-anh">{{trans('home.gallery.viewall')}}<i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Gallery  Section Start -->