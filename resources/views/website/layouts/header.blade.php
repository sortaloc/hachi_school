<!--Full width header Start-->
<div class="full-width-header header-style3 home11-modify2">
    <!--Header Start-->
    <header id="rs-header" class="rs-header">
        <!-- Topbar Area Start -->
        <div class="topbar-area">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-md-8">
                        <ul class="topbar-contact">
                            <li>
                                <a href="https://www.google.com/maps/place/87+Nguy%E1%BB%85n+Th%E1%BB%8B+Minh+Khai,+H%C6%B0ng+B%C3%ACnh,+Th%C3%A0nh+ph%E1%BB%91+Vinh,+Ngh%E1%BB%87+An,+Vi%E1%BB%87t+Nam/@18.674505,105.678425,17z/data=!4m5!3m4!1s0x3139ce706fd0344b:0x6f0b06a681d64b03!8m2!3d18.6745047!4d105.6784254?hl=vi" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    {{trans('home.footer.address.address')}}
                                </a>
                            </li>
                            <li>
                                <a href="tel:{{trans('home.footer.address.tel')}}">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    {{trans('home.footer.address.tel')}}
                                </a>
                            </li>
                            <li>
                                <a href="mailto:{{trans('home.footer.address.email')}}">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    {{trans('home.footer.address.email')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 text-right">
                        <ul class="topbar-right">
                            <li>
                                <a href="/dang-nhap">
                                    <i class="fa fa-sign-in" aria-hidden="true"></i>
                                    {{trans('admin.login')}}
                                </a>
                            </li>
                            <li>
                                <a href="/dang-ky">
                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                    {{trans('home.menu.register')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Topbar Area End -->
        <!-- Menu Start -->
        <div class="menu-area menu-sticky">
            <div class="logo-part hidden-md">
                <a class="light-logo" href="/">
                    <img src="/home/images/logo/logo-dark3.png" alt="">
                </a>
                <a class="small-logo" href="/">
                    <img src="/home/images/logo/logo-dark3.png" alt="">
                </a>
            </div>
            <div class="container-fluid">
                <div class="responsive-logo">
                    <img src="/home/images/logo/logo-dark3.png" alt="">
                </div>
                <div class="rs-menu-area">
                    <div class="main-menu">
                        <div class="mobile-menu">
                            <a class="rs-menu-toggle">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>
                        <nav class="rs-menu rs-menu-close">
                            <ul class="nav-menu">
                                <li>
                                    <a href="/">{{trans('home.menu.home')}}</a>
                                </li>
                                <li>
                                    <a href="/gioi-thieu">{{trans('home.menu.about')}}</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/tin-tuc">{{trans('home.menu.news.main')}}<i style="margin-left:5px" class="d-none d-lg-inline fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="sub-menu" style="width:150px;">
                                        <li><a href="/tin-tuc/tin-nha-truong">{{trans('home.menu.news.sub_1')}}</a> </li>
                                        <li><a href="/tin-tuc/tin-tu-so-giao-duc">{{trans('home.menu.news.sub_2')}}</a> </li>
                                        <li><a href="/tin-tuc/tin-tu-bo-giao-duc">{{trans('home.menu.news.sub_3')}}</a> </li>
                                        <li><a href="/tin-tuc/hoc-duong">{{trans('home.menu.news.sub_4')}}</a> </li>
                                        <li><a href="/tin-tuc/hoc-sinh">{{trans('home.menu.news.sub_5')}}</a> </li>
                                        <li><a href="/tin-tuc/giai-thuong">{{trans('home.menu.news.sub_6')}}</a> </li>
                                        <li><a href="/tin-tuc/hoc-bong">{{trans('home.menu.news.sub_7')}}</a> </li>
                                        <li><a href="/tin-tuc/truong-lien-ket">{{trans('home.menu.news.sub_8')}}</a> </li>
                                        <li><a href="/tin-tuc/thi-tuyen-sinh">{{trans('home.menu.news.sub_9')}}</a> </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="/tai-nguyen">{{trans('home.menu.resources.main')}}<i style="margin-left:5px" class="d-none d-lg-inline fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul class="sub-menu" style="width:150px;">
                                        <li><a href="/tai-nguyen/thu-vien-anh">{{trans('home.menu.resources.sub_1')}}</a> </li>
                                        <li><a href="/tai-nguyen" style="pointer-events: none;cursor: default;opacity: 0.6;">{{trans('home.menu.resources.sub_2')}}</a> </li>
                                        <li><a href="/tai-nguyen" style="pointer-events: none;cursor: default;opacity: 0.6;">{{trans('home.menu.resources.sub_3')}}</a> </li>
                                        <li><a href="/tai-nguyen" style="pointer-events: none;cursor: default;opacity: 0.6;">{{trans('home.menu.resources.sub_4')}}</a> </li>
                                        <li><a href="/tai-nguyen" style="pointer-events: none;cursor: default;opacity: 0.6;">{{trans('home.menu.resources.sub_5')}}</a> </li>
                                        <li><a href="/tai-nguyen" style="pointer-events: none;cursor: default;opacity: 0.6;">{{trans('home.menu.resources.sub_6')}}</a> </li>
                                    </ul>
                                    <!-- <ul class="sub-menu">
                                        <li class="menu-item-has-children">
                                            <a href="/tai-nguyen#">Team</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tai-nguyen#">Team One</a></li>
                                                <li><a href="/tai-nguyen#">Team Two</a></li>
                                            </ul>
                                            <span class="rs-menu-parent"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="/tai-nguyen#">Event</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tai-nguyen#">Event One</a></li>
                                                <li><a href="/tai-nguyen#">Event Two</a></li>
                                                <li><a href="/tai-nguyen#">Event Three</a></li>
                                            </ul>
                                            <span class="rs-menu-parent"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                        </li>
                                        <li class="menu-item-has-children">
                                            <a href="/tai-nguyen#">Gallery</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tai-nguyen#">Gallery One</a></li>
                                                <li><a href="/tai-nguyen#">Gallery Two</a></li>
                                                <li><a href="/tai-nguyen#">Gallery Three</a></li>
                                            </ul>
                                            <span class="rs-menu-parent"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                        </li>
                                    </ul>
                                    <span class="rs-menu-parent"><i class="fa fa-angle-down" aria-hidden="true"></i></span> -->
                                </li>
                                <li>
                                    <a href="/su-kien">{{trans('home.menu.event')}}</a>
                                </li>
                                <li>
                                    <a href="/lien-he">{{trans('home.menu.contact')}}</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a>
                                    @if(Lang::locale() === 'vn')
                                        <i class="flag-icon flag-icon-vn"></i>
                                    @else
                                        <i class="flag-icon flag-icon-gb"></i>
                                    @endif
                                        {{trans('home.menu.lang')}}
                                        <i class="d-none d-lg-inline fa fa-caret-down" aria-hidden="true"></i>
                                    </a>
                                    <ul class="sub-menu lang">
                                        <li><a href="{{url('language/vn')}}"><i class="flag-icon flag-icon-vn"></i>&nbsp;&nbsp;{{trans('home.lang.vn')}}</a></li>
                                        <li><a href="{{url('language/en')}}"><i class="flag-icon flag-icon-gb"></i>&nbsp;&nbsp;{{trans('home.lang.en')}}</a></li>
                                    </ul>
                                </li>
                                <li class="d-inline d-md-none">
                                    <a href="/dang-nhap">{{trans('admin.login')}}</a>
                                </li>
                                <li class="d-inline d-md-none">
                                    <a href="/dang-ky">{{trans('home.menu.register')}}</a>
                                </li>
                                <li class="d-inline d-md-none">
                                    <a data-target=".search-modal" data-toggle="modal" href="#">
                                        <i class="fa fa-search" style="margin-right:5px;"></i> Tìm kiếm
                                    </a>
                                </li>
                            </ul> <!-- //.nav-menu -->
                        </nav>                                        
                    </div> <!-- //.main-menu -->
                    <div class="expand-btn-inner">
                        <ul>
                            <li>
                                <a class="hidden-xs rs-search" data-target=".search-modal" data-toggle="modal" href="#">
                                    <i class="flaticon-search"></i>
                                </a>
                            </li>
                            <li>
                                <a class="hachi-btn apply-btn" href="#" data-toggle="tooltip" data-placement="top" title="{{trans('home.apply_now')}}">{{trans('home.apply_now')}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Menu End -->
    </header>
    <!--Header End-->
</div>
<!--Full width header End-->