    <!-- About video Section Start -->
    <div id="rs-about-video" class="rs-about-video pb-50 md-pb-30">
        <div class="container text-center">
            <div class="video-img-part media-icon orange-color2">
                <img src="home/images/about/about-video-bg-orange.png" alt="Video Bg Image">
                <a class="popup-videos" href="https://www.youtube.com/watch?v=nbAAPrU_V08">
                    <i class="fa fa-play"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- About video Section End -->