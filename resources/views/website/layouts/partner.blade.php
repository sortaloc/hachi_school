    <!-- Partner Start -->
    <div class="rs-partner gray-bg3 pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="sec-title2 text-center mb-25 md-mb-15">
                <!-- <div class="sub-title">{{trans('home.list')}}</div> -->
                <h2 class="title">{{trans('home.partners')}}</h2>
            </div>
            <div class="rs-carousel owl-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="true" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="4" data-md-device-nav="true" data-md-device-dots="false">
                <div class="partner-item">
                    <a href="#"><img src="/home/images/partner/1.png" alt=""></a>
                </div>
                <div class="partner-item">
                    <a href="#"><img src="/home/images/partner/2.png" alt=""></a>
                </div>
                <div class="partner-item">
                    <a href="#"><img src="/home/images/partner/3.png" alt=""></a>
                </div>
                <div class="partner-item">
                    <a href="#"><img src="/home/images/partner/4.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Partner End --> 