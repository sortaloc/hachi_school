<!-- Footer Start -->
<footer id="rs-footer" class="rs-footer home11-style">
    <div class="magic"></div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 mb-30 md-mb-25">
                    <div class="footer-logo md-text-center">
                        <a href="/"><img src="/home/images/logo/logo-light2.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 footer-widget md-mb-25">
                    <h4 class="widget-title">{{trans('home.menu.about')}}</h4>
                    <ul class="site-map">
                        <li><a href="#">{{trans('home.footer.about.sbj1')}}</a></li>
                        <li><a href="#">{{trans('home.footer.about.sbj2')}}</a></li>
                        <li><a href="#">{{trans('home.footer.about.sbj3')}}</a></li>
                        <li><a href="#">{{trans('home.footer.about.sbj4')}}</a></li>
                        <li><a href="#">{{trans('home.footer.about.sbj5')}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-widget md-mb-25">
                    <h4 class="widget-title">{{trans('home.contact.admissions')}}</h4>
                    <ul class="site-map">
                        <li><a href="#">{{trans('home.footer.admissions.sbj1')}}</a></li>
                        <li><a href="#">{{trans('home.footer.admissions.sbj2')}}</a></li>
                        <li><a href="#">{{trans('home.footer.admissions.sbj3')}}</a></li>
                        <li><a href="#">{{trans('home.footer.admissions.sbj4')}}</a></li>
                        <li><a href="#">{{trans('home.footer.admissions.sbj5')}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-widget sm-mb-25">
                    <h4 class="widget-title">{{trans('home.footer.program.title')}}</h4>
                    <ul class="site-map">
                        <li><a href="#">{{trans('home.footer.program.sbj1')}}</a></li>
                        <li><a href="#">{{trans('home.footer.program.sbj2')}}</a></li>
                        <li><a href="#">{{trans('home.footer.program.sbj3')}}</a></li>
                        <li><a href="#">{{trans('home.footer.program.sbj4')}}</a></li>
                        <li><a href="#">{{trans('home.footer.program.sbj5')}}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-widget">
                    <h4 class="widget-title">{{trans('home.menu.contact')}}</h4>
                    <ul class="address-widget">
                        <li>
                            <div class="desc">
                                <a href="https://www.google.com/maps/place/87+Nguy%E1%BB%85n+Th%E1%BB%8B+Minh+Khai,+H%C6%B0ng+B%C3%ACnh,+Th%C3%A0nh+ph%E1%BB%91+Vinh,+Ngh%E1%BB%87+An,+Vi%E1%BB%87t+Nam/@18.674505,105.678425,17z/data=!4m5!3m4!1s0x3139ce706fd0344b:0x6f0b06a681d64b03!8m2!3d18.6745047!4d105.6784254?hl=vi" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
                                    {{trans('home.footer.address.address')}}
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="desc">
                                <a href="tel:{{trans('home.footer.address.tel')}}">
                                    <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
                                    {{trans('home.footer.address.tel')}}
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="desc">
                                <a href="mailto:{{trans('home.footer.address.email')}}">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;
                                    {{trans('home.footer.address.email')}}
                                </a>
                            </div>
                        </li>
                    </ul>
                    <ul class="footer-social">
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">                    
            <div class="row y-middle">
                <div class="col-lg-12">
                    <div class="copyright text-center md-text-left">
                        <p>Copyright 2020 HachiSchool. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="w-100">
        <iframe src="https://educavo.000webhostapp.com/cat" frameborder="0" width="100%" style="min-height: 700px;"></iframe>
    </div> -->
</footer>
<!-- Footer End -->