<!-- Register Section -->
<section class="register-section pt-50 pb-50">
    <div class="container">
        <div class="register-box">
            <div class="sec-title text-center mb-30">
                <h2 class="title mb-10">{{trans('home.new_account.create')}}</h2>
            </div>
            <!-- Login Form -->
            <div class="styled-form">
                <div id="form-messages"></div>
                <form id="contact-form" method="post" action="mailer.php">                               
                    <div class="row clearfix">                                    
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="fname" name="fname" value="" placeholder="" required="required">
                            <span class="placeholder">{{trans('home.new_account.first_name')}} <span></span></span>
                        </div>
                        <div class="form-group col-lg-12">
                            <input type="text" id="lname" name="lname" value="" placeholder="" required="required">
                            <span class="placeholder">{{trans('home.new_account.last_name')}} <span></span></span>
                        </div>
                        <div class="form-group col-lg-12">
                            <input type="email" id="email" name="email" value="" placeholder="" required="required">
                            <span class="placeholder">{{trans('home.contact.emailaddresss')}} <span></span></span>
                        </div>
                        <div class="form-group col-lg-12">
                            <input type="text" id="Username" name="username" value="" placeholder="" required="required">
                            <span class="placeholder">{{trans('home.new_account.username')}} <span></span></span>
                        </div>    
                        <div class="form-group col-lg-12">
                            <input type="text" id="Username" name="password" value="" placeholder="" required="required">
                            <span class="placeholder">{{trans('admin.password')}} <span></span></span>
                        </div>    
                        <div class="form-group col-lg-12">
                            <input type="text" id="Username" name="confirm_password" value="" placeholder="" required="required">
                            <span class="placeholder">{{trans('admin.confirm')}} {{trans('admin.password')}} <span></span></span>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <div class="row clearfix">
                                <div class="column col-lg-3 col-md-4 col-sm-12">
                                    <div class="radio-box">
                                        <input type="radio" name="remember-password" id="type-1"> 
                                    </div>
                                </div>
                                <div class="column col-lg-3 col-md-4 col-sm-12">
                                    <div class="radio-box">
                                        <input type="radio" name="remember-password" id="type-2"> 
                                    </div>
                                </div>
                                <div class="column col-lg-3 col-md-4 col-sm-12">
                                    <div class="radio-box">
                                        <input type="radio" name="remember-password" id="type-3"> 
                                    </div>
                                </div>
                                <div class="column col-lg-12 col-md-12 col-sm-12">
                                    <div class="check-box">
                                        <input type="checkbox" name="remember-password" id="type-4"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-center">
                            <button type="submit" class="hachi-btn" data-toggle="tooltip" data-placement="top" title="{{trans('home.new_account.sign_up')}}"><span class="txt">{{trans('home.new_account.sign_up')}}</span></button>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <div class="users">{{trans('home.new_account.have_an_acc')}} <a href="/dang-nhap">{{trans('home.new_account.sign_in')}}</a></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End Login Section --> 