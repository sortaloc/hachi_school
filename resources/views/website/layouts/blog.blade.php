    <!-- Blog Section Start -->
    <div id="rs-blog" class="rs-blog style1 home11-style pt-50 pb-20 md-pt-30 md-pb-10">
        <div class="container">
            <div class="sec-title2 text-center mb-25 md-mb-15">
                <!-- <div class="sub-title">{{trans('home.menu.news.main')}} &amp; {{trans('home.menu.event')}}</div> -->
                <h2 class="title">{{trans('home.blog.latest1')}}{{trans('home.menu.news.main')}} &amp; {{trans('home.menu.event')}}{{trans('home.blog.latest2')}}</h2>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-12 pr-75 md-pr-15 md-mb-20">
                    <div class="row no-gutter white-bg blog-item mb-15">
                        <div class="col-md-6">
                            <div class="image-part">
                                <a href="#"><img src="/home/images/gallery/1.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="blog-content">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user-o"></i> Admin</li>
                                    <li><i class="fa fa-calendar"></i>Nov 15, 2020</li>
                                </ul>
                                <h3 class="title"><a href="#">{{trans('home.blog.news1')}}</a></h3>
                                <div class="btn-part">
                                    <a class="readon-arrow" href="#">{{trans('home.about.read_more')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutter white-bg blog-item mb-15">
                        <div class="col-md-6 d-block d-md-none">
                            <div class="image-part">
                                <a href="#"><img src="/home/images/gallery/2.jpg" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="blog-content">
                                <ul class="blog-meta">
                                    <li><i class="fa fa-user-o"></i> Admin</li>
                                    <li><i class="fa fa-calendar"></i>Nov 15, 2020</li>
                                </ul>
                                <h3 class="title"><a href="#">{{trans('home.blog.news2')}}</a></h3>
                                <div class="btn-part">
                                    <a class="readon-arrow" href="#">{{trans('home.about.read_more')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 d-none d-md-block">
                            <div class="image-part">
                                <a href="#"><img src="/home/images/gallery/2.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>                         
                </div>
                <div class="col-lg-5 lg-pl-0">
                    <div class="events-short new-style mb-10 wow" style="visibility: hidden; animation-name: none;">
                        <div class="date-part">
                            <span class="month">Dec</span>
                            <div class="date">26</div>
                        </div>
                        <div class="content-part new-padding">
                            <div class="categorie">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 7:00 AM -  05:00 PM</div>
                            </div>
                            <h4 class="title text-justify mb-10"><a href="#">{{trans('home.blog.event1')}}</a></h4>
                        </div>
                    </div>
                    <div class="events-short new-style mb-10">
                        <div class="date-part">
                            <span class="month">Dec</span>
                            <div class="date">12</div>
                        </div>
                        <div class="content-part new-padding">
                            <div class="categorie">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 7:00 AM -  05:00 PM</div>
                            </div>
                            <h4 class="title text-justify mb-10"><a href="#">{{trans('home.blog.event2')}}</a></h4>
                        </div>
                    </div>
                    <div class="events-short new-style mb-10">
                        <div class="date-part">
                            <span class="month">Nov</span>
                            <div class="date">18</div>
                        </div>
                        <div class="content-part new-padding">
                            <div class="categorie">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 7:00 AM -  05:00 PM</div>
                            </div>
                            <h4 class="title text-justify mb-10"><a href="#">{{trans('home.blog.event3')}}</a></h4>
                        </div>
                    </div>
                    <div class="events-short new-style mb-10">
                        <div class="date-part">
                            <span class="month">Oct</span>
                            <div class="date">06</div>
                        </div>
                        <div class="content-part new-padding">
                            <div class="categorie">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 4:00 PM -  05:00 PM</div>
                            </div>
                            <h4 class="title text-justify mb-10"><a href="#">{{trans('home.blog.event4')}}</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog Section End -->