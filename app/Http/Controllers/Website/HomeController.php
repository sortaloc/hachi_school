<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
    public function login() {
        $breadcrumb['title'] = 'home.login.my_account';
        return view('website.content.login', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function register() {
        $breadcrumb['title'] = 'home.menu.register';
        return view('website.content.register', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function index() {
        $breadcrumb['title'] = 'home.menu.home';
        return view('website.content.home', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function about() {
        $breadcrumb['title'] = 'home.menu.about';
        return view('website.content.about', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['subtitle'] = 'home.menu.news.main';
        return view('website.content.news', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub1() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_1';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub2() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_2';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub3() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_3';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub4() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_4';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub5() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_5';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub6() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_6';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub7() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_7';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub8() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_8';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function news_sub9() {
        $breadcrumb['title'] = 'home.menu.news.main';
        $breadcrumb['titlehref'] = 'home.menu.news.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.news.sub_9';
        return view('website.content.news_sub1', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function resources() {
        $breadcrumb['title'] = 'home.menu.resources.main';
        return view('website.content.resources', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function resources_gallery() {
        $breadcrumb['title'] = 'home.menu.resources.main';
        $breadcrumb['titlehref'] = 'home.menu.resources.titlehref';
        $breadcrumb['subtitle'] = 'home.menu.resources.sub_1';
        return view('website.content.gallery', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function event() {
        $breadcrumb['title'] = 'home.menu.event';
        return view('website.content.event', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
    public function contact() {
        $breadcrumb['title'] = 'home.menu.contact';
        return view('website.content.contact', [
            'breadcrumb' => (object)$breadcrumb
        ]);
    }
}
